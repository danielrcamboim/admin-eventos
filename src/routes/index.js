import { Switch, Route } from "react-router-dom"
import Home from "../pages/Home"
import Formatura from "../pages/Formatura"
import Casamento from "../pages/Casamento"
import Confra from "../pages/Confraternizacao"

const Routes = () => {
    return (
        <Switch>
            <Route exact path="/">
                <Home/>
            </Route>

            <Route path="/formatura">
                <Formatura/>
            </Route>

            <Route path="/casamento">
                <Casamento/>
            </Route>

            <Route path="/confraternizacao">
                <Confra/>
            </Route>
        </Switch>
    )
}

export default Routes