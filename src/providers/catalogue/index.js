import { createContext, useEffect, useState } from "react"
import axios from "axios"

export const CatalogueContext = createContext([])

export const CatalogueProvider = ({children}) => {

    const [catalogue, setCatalogue] = useState([])

    const getCataloguefromApi = () => {
        axios
        .get("https://api.punkapi.com/v2/beers")
        .then((response) => setCatalogue(response.data))
    }

    useEffect(() => {
        getCataloguefromApi()
    }, [])

    return (
        <CatalogueContext.Provider value={{catalogue, getCataloguefromApi}}>
            {children}
        </CatalogueContext.Provider>
    )
}

