import { createContext, useState, useEffect } from "react"

export const FormaturaContext = createContext()

export const FormaturaProvider = ({children}) => {
    
    const [formatura, setFormatura] = useState(
        JSON.parse(localStorage.getItem("formatura")) || []
    )

    useEffect(() => {
        localStorage.setItem("formatura", JSON.stringify(formatura))
    }, [formatura])
    
    return (
        <FormaturaContext.Provider value={{formatura, setFormatura}}>
            {children}
        </FormaturaContext.Provider>
    )
}