import { createContext, useEffect, useState } from "react"

export const CasamentoContext = createContext()

export const CasamentoProvider = ({children}) => {

    const [casamento, setCasamento] = useState(
        JSON.parse(localStorage.getItem("casamento")) || []
    )

    useEffect(() => {
        localStorage.setItem("casamento", JSON.stringify(casamento))
    }, [casamento])

    return (
        <CasamentoContext.Provider value={{casamento, setCasamento}}>
            {children}
        </CasamentoContext.Provider>
    )
}

