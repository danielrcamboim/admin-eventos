import { createContext, useState, useEffect } from "react"

export const ConfraternizacaoContext = createContext()

export const ConfraternizacaoProvider = ({children}) => {

    const [confraternizacao, setConfraternizacao] = useState(
        JSON.parse(localStorage.getItem("confraternizacao")) || []
    )

    useEffect(() => {
        localStorage.setItem("confraternizacao", JSON.stringify(confraternizacao))
    }, [confraternizacao])

    return (
        <ConfraternizacaoContext.Provider value={{confraternizacao, setConfraternizacao}}>
            {children}
        </ConfraternizacaoContext.Provider>
    )
}