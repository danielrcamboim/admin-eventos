import { CasamentoProvider } from "./casamento"
import { CatalogueProvider } from "./catalogue"
import { FormaturaProvider } from "./formatura"
import { ConfraternizacaoProvider } from "./confraternizacao" 

const Providers = ({children}) => {
    return (
        <CatalogueProvider>
            <CasamentoProvider>
                <FormaturaProvider>
                    <ConfraternizacaoProvider>
                        {children}
                    </ConfraternizacaoProvider>
                </FormaturaProvider>
            </CasamentoProvider>
        </CatalogueProvider>
    )
}

export default Providers