import { useContext } from "react"
import CardProd from "../../components/Card"
import { ConfraternizacaoContext } from "../../providers/confraternizacao"

const Confra = () => {
    
    const { confraternizacao } = useContext(ConfraternizacaoContext)
    
    return (
        <div style={{display: "flex", flexWrap: "wrap", justifyContent: "center"}}>
            { confraternizacao.map((item) => <CardProd key={item.id} item={item} isRemovable type="confra" />) }
        </div>
    ) 
}

export default Confra
