import { useContext } from "react"
import { FormaturaContext } from "../../providers/formatura"
import CardProd from "../../components/Card"

const Formatura = () => {
    
    const { formatura } = useContext(FormaturaContext)
    
    return (
        <div style={{display: "flex", flexWrap: "wrap", justifyContent: "center"}}>
            { formatura.map((item) => <CardProd key={item.id} item={item} isRemovable type="formatura"/>) }
        </div>
    )
}

export default Formatura