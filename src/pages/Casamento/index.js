import { useContext } from "react"
import { CasamentoContext } from "../../providers/casamento"
import CardProd from "../../components/Card"

const Casamento = () => {
    
    const { casamento } = useContext(CasamentoContext)
    
    return (
        <div style={{display: "flex", flexWrap: "wrap", justifyContent: "center"}}>
            { casamento.map((item) => <CardProd key={item.id} item={item} isRemovable type="casamento" />) }
        </div>
    )
}

export default Casamento