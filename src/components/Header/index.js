import React, { useContext } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { Link } from "react-router-dom"
import Button from '@material-ui/core/Button'
import Badge from '@material-ui/core/Badge'
import { ConfraternizacaoContext } from '../../providers/confraternizacao'
import { CasamentoContext } from '../../providers/casamento'
import { FormaturaContext } from '../../providers/formatura'


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  link: {
    textDecoration: "none"
  }
}));

export default function ButtonAppBar() {
  const classes = useStyles();

  const { confraternizacao } = useContext(ConfraternizacaoContext)
  const { casamento } = useContext(CasamentoContext)
  const { formatura } = useContext(FormaturaContext)

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            KenzieBeer
          </Typography>
          <Link to="/" className={classes.link}>
            <Button variant="contained" color="primary">
              Catálogo
            </Button>
          </Link>
          <Link to="/formatura" className={classes.link}>
            <Badge badgeContent={formatura.length} color="secondary">
              <Button variant="contained" color="primary">
                Formatura
              </Button>
            </Badge>
          </Link>
          <Link to="/casamento" className={classes.link}>
            <Badge badgeContent={casamento.length} color="secondary">
              <Button variant="contained" color="primary">
                Casamento
              </Button>
            </Badge>
          </Link>
          <Link to="/confraternizacao" className={classes.link}>
            <Badge badgeContent={confraternizacao.length} color="secondary">
              <Button variant="contained" color="primary">
                Confraternização
              </Button>
            </Badge>
          </Link>
        </Toolbar>
      </AppBar>
    </div>
  );
}