import { useContext } from "react"
import { CatalogueContext } from "../../providers/catalogue"
import CardProd from "../Card"

const CatalogueList = () => {
    
    const { catalogue } = useContext(CatalogueContext)

    return (
        <div style={{display: "flex", flexWrap: "wrap", justifyContent: "center"}}>
            { catalogue.map((item) => <CardProd key={item.id} item={item}/>) }
        </div>
    )
}

export default CatalogueList