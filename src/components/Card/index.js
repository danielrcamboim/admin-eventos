import { useContext, useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import Card from '@material-ui/core/Card'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import Collapse from '@material-ui/core/Collapse'
import IconButton from '@material-ui/core/IconButton'
import Typography from '@material-ui/core/Typography'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import Button from '@material-ui/core/Button'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'

import { CasamentoContext } from "../../providers/casamento"
import { FormaturaContext } from "../../providers/formatura"
import { ConfraternizacaoContext } from "../../providers/confraternizacao"

const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: 345,
    width: 350,
    marginTop: 15,
    marginLeft: 2.5,
    marginRight: 2.5
  },
  media: {
    height: 0,
    paddingTop: '56.25%',
  },
  expand: {
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
}));

const CardProd = ({item, isRemovable = false, type}) => {
  
  const classes = useStyles();
  const [expanded, setExpanded] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);

  const { name, first_brewed, description, image_url } = item
  const { casamento, setCasamento } = useContext(CasamentoContext)
  const { formatura, setFormatura } = useContext(FormaturaContext)
  const { confraternizacao, setConfraternizacao } = useContext(ConfraternizacaoContext)

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null)
  }

  
  const remove = (name, type) => {
    
    switch (true) {
      case type === "casamento":
        const listaCasamento = casamento.filter(item => item.name !== name)
        localStorage.setItem("casamento", JSON.stringify(listaCasamento))
        setCasamento(listaCasamento)
      break

      case type === "formatura":
        const listaFormatura = formatura.filter(item => item.name !== name)
        localStorage.setItem("formatura", JSON.stringify(listaFormatura))
        setFormatura(listaFormatura)
      break

      case type === "confra":
        const listaConfra = confraternizacao.filter(item => item.name !== name)
        localStorage.setItem("confraternizacao", JSON.stringify(listaConfra))
        setConfraternizacao(listaConfra)
        break

      default:
        return
    }
  }

  return (
    <Card className={classes.root} variant="outlined">
      
      <CardMedia
        className={classes.media}
        image={image_url}
        title={name}
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          {name}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          {first_brewed}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        
        { isRemovable ? <Button onClick={() => remove(item.name, type)}>Remover</Button> :
          <>        
          <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
              Adicionar
          </Button>
          <Menu
              id="simple-menu"
              anchorEl={anchorEl}
              keepMounted
              open={Boolean(anchorEl)}
              onClose={handleClose}
          >
              <MenuItem onClick={() => {setFormatura([...formatura, item]); setAnchorEl(null)}}>Formatura</MenuItem>
              <MenuItem onClick={() => {setCasamento([...casamento, item]); setAnchorEl(null)}}>Casamento</MenuItem>
              <MenuItem onClick={() => {setConfraternizacao([...confraternizacao, item]); setAnchorEl(null)}}>Confraternizaçao</MenuItem>
          </Menu>
          </>
        }  
          <IconButton
            className={clsx(classes.expand, {
              [classes.expandOpen]: expanded,
            })}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography paragraph>Descrição:</Typography>
          <Typography paragraph>
            {description}
          </Typography>
        </CardContent>
      </Collapse>
    </Card>
  );
}

export default CardProd

